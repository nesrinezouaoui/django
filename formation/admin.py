from django.contrib import admin

from formation.models import Formation

admin.site.register(Formation)
