from django.shortcuts import render, redirect, get_object_or_404
from django.db.models import Q
from formation.models import Formation
from account.models import Account
from formation.forms import CreateFormationForm, UpdateFormationPostForm
from personal.filters import OrderFilter



def create_formation_view(request):

	context = {}

	user = request.user
	if not user.is_authenticated:
		return redirect("must_authenticate")

	form = CreateFormationForm(request.POST or None, request.FILES or None)
	if form.is_valid():
		obj = form.save(commit=False)
		 
		obj.save()
		form = CreateFormationForm()
	context['form'] = form



	return render(request, "formation/create_formation.html", context)


def detail_formation_view(request, slug):

	context = {}

	formation_posts = get_object_or_404(Formation, slug=slug)

	context['formation_posts'] = formation_posts


	return render (request, 'formation/detail_formation.html', context)


def edit_formation_view(request, slug):


	context = {}

	user = request.user

	if not user.is_authenticated:
		return redirect("must_authenticate")

	formation_posts = get_object_or_404(Formation, slug=slug)
	if request.POST:
		form = UpdateFormationPostForm(request.POST or None, request.FILES or None, instance=formation_posts)
		if form.is_valid():
			obj = form.save(commit=False)
			obj.save()
			context['success_message'] = "Updated"
			formation_posts = obj

	form = UpdateFormationPostForm(
			initial = {

					"title": formation_posts.title,
					"etat": formation_posts.etat,
					"logo": formation_posts.logo,
			}



		)

	context['form'] = form
	return render(request, 'formation/edit_formation.html', context)


def get_formation_queryset(query=None):
	queryset = []
	queries = query.split(" ")
	for q in queries:
		posts = Formation.objects.filter(
			Q(title__icontains=q)|
			Q(etat__icontains=q)

			).distinct()
		for post in posts:
				queryset.append(post)
	return list(set(queryset))




def search(request):
    user_list = Formation.objects.all()
    user_filter = OrderFilter(request.GET, queryset=user_list)
    return render(request, 'formation/detail_formation.html', {'filter': user_filter})
