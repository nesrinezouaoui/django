from django import forms

from formation.models import Formation

class CreateFormationForm(forms.ModelForm):

	class Meta:
		model = Formation
		fields = ['title','logo' ,'etat']



class UpdateFormationPostForm(forms.ModelForm):

	class Meta:
		model = Formation
		fields = ['title', 'logo', 'etat']

		def save(self, commit=True):
			formation_posts = self.instance
			formation_posts.title = self.cleaned_data['title']
			formation_posts.etat = self.cleaned_data['etat']

			if self.cleaned_data['logo']:
				formation_posts.logo = self.cleaned_data['logo']

			if commit:
				formation_posts.save()
			return formation_posts






