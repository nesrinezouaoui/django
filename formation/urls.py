from django.urls import path
from formation.models import Formation
from formation.views import(

		create_formation_view,
		detail_formation_view,
		edit_formation_view,
		search,
		
		

	)

app_name = 'formation'

urlpatterns =[

path('create/' , create_formation_view, name="create"),
path('<slug>/' ,detail_formation_view, name="detail"),
path('<slug>/edit/' ,edit_formation_view, name="edit"),
path('search/', search, name='search'),


]