from django.db import models
from django.utils.text import slugify
import uuid
from django.db.models.signals import post_delete, pre_save
from django.dispatch import receiver

#Two tuple structure
#The first element in each tuple is the value that will be stored in the database. The second element is displayed by the field’s form widget.
#Tuple
ETAT = [
	('Active', 'Active'), #Tuple1
	('Non Active', 'Non Active'), #Tuple2
	
]
TYPES = [
	('JAVA', 'JAVA'), #Tuple1
	('HTML', 'HTML'), #Tuple2
	
]

class Formation(models.Model):
	title		 			= models.CharField(max_length=60)
	logo 					= models.ImageField(upload_to='images/')
	etat 					= models.CharField(max_length=10, choices=ETAT)
	types 					= models.CharField(max_length=10, choices=TYPES)
	slug 					= models.SlugField(blank=True, unique=True)

	def __str__(self):
		return self.title


	class Meta:
		verbose_name = 'Formation'
		verbose_name_plural = 'Formation chez GTA'

@receiver(post_delete, sender=Formation)
def submission_delete(sender, instance, **kwargs):
    instance.image.delete(False) 

def pre_save_blog_post_receiver(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = slugify(instance.title)

pre_save.connect(pre_save_blog_post_receiver, sender=Formation)