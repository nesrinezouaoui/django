import django_filters

from formation.models import Formation


class OrderFilter(django_filters.FilterSet):
	
	class Meta:
		model = Formation
		fields = ['etat']

