from django.shortcuts import render
from formation.models import Formation
from formation.views import get_formation_queryset
from operator import attrgetter


# Create your views here.
def home_screen_view(request):

	  context = {}

	  query = ""
	  if request.GET:
	  	query = request.GET['q']
	  	context['query'] = str(query)


	  formation_posts = get_formation_queryset(query)
	  context['formation_posts'] = formation_posts


	  return render(request, "personal/home.html", context)
      




